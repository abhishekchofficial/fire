<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Icon Pay | Dashboard</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{url('public/assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{url('public/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/assets/global/plugins/morris/morris.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/assets/global/plugins/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/assets/global/plugins/jqvmap/jqvmap/jqvmap.css')}}" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{url('public/assets/global/css/components-md.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{url('public/assets/global/css/plugins-md.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{url('public/assets/layouts/layout2/css/layout.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/assets/layouts/layout2/css/themes/blue.min.css')}}" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{url('public/assets/layouts/layout2/css/custom.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> 
        <link href="{{url('public/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('public/assets/pages/css/invoice-2.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('public/assets/custom/custom-style.css')}}" rel="stylesheet" type="text/css" />

		<link href="{{url('public/assets/admin/admin-custom.css')}}" rel="stylesheet" type="text/css" />

		<link href="{{url('public/assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('public/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet')}}" type="text/css" />	
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
	
		<meta name="csrf-token" content="{{ csrf_token() }}">
		
		<script>
			window.Laravel = <?php echo json_encode([
				'csrfToken' => csrf_token(),
			]); ?>
		</script>	
		
		
		
		<style>
				.page-sidebar-wrapper ul li a.router-link-exact-active{
				color:#18cdc4;
				    background: #212d41;
				}
		
				.page-sidebar-wrapper ul li a.router-link-exact-active i{  
				color:#18cdc4 !important;
				}

				.page-sidebar .page-sidebar-menu li a i {
					float: left;
					font-size: 16px;
					margin: 0 7px 0 0 !important;
				}
				.page-sidebar .page-sidebar-menu li a {
					min-height: auto;
					padding: 10px 5px 10px 5px;
					float: left;
					width: 100%;
				}
				.page-sidebar .page-sidebar-menu li a .title {
					float: left;
					margin: 0;
					font-size: 12px;
				}
				.page-sidebar .page-sidebar-menu li a .arrow {
					float: right;
				}
				.page-sidebar .page-sidebar-menu li a .arrow::before {
					margin: 0;
				}
				.page-content-wrapper .page-content {
					overflow: hidden;
				}
				
        </style>

	</head>
    <!-- END HEAD -->