<style type="text/css"> 
.page-sidebar .page-sidebar-menu li a .title {
   width: 80%;
   text-align: left;
}
.form-md-floating-label .select2-multiple{
    border-bottom: 1px solid #327ad5 !important;
    height:56px !important;
    width:100% !important;
}
.page-sidebar .page-sidebar-menu li:nth-child(10) a .title{
    width: 84%;
}
</style>
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">               
            <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 0px">
         
         <li class="nav-item start @php if(Request::url()==URL::to('admin/home')){echo 'active';} @endphp ">                   
                <a href="{{url('admin/home')}}">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                </a>
            </li>                   
            <li class="nav-item start @php if(Request::url()==URL::to('admin/merchant-list')){echo 'active';} @endphp ">
                <a href="{{url('admin/merchant-list')}}">
                    <i class="icon-user"></i>
                    <span class="title">Merchant's List</span>
                    <span class="selected"></span>
                </a>
            </li>

            <li class="nav-item start @php if(Request::url()==URL::to('admin/inventory-list')){echo 'active';} @endphp ">
                <a href="{{url('admin/inventory-list')}}">
                    <i class="icon-user"></i>
                    <span class="title">Inventory</span>
                    <span class="selected"></span>
                </a>
            </li>

            <li class="nav-item start @php if(Request::url()==URL::to('admin/reports')){echo 'active';} @endphp ">
                <a href="{{url('admin/reports')}}">
                    <i class="icon-user"></i>
                    <span class="title">Download Report</span>
                    <span class="selected"></span>
                </a>
            </li>
    
            <li class="nav-item start @php if(Request::url()==URL::to('admin/transaction-reverse')){echo 'active';} @endphp ">
                <a href="{{url('admin/transaction-reverse')}}">
                    <i class="icon-user"></i>
                    <span class="title">Reverse Transaction Report</span>
                    <span class="selected"></span>
                </a>
            </li>    
            
    


                                                             
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>