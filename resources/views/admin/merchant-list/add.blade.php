<style type="text/css">
.user_form_group .form-group{
	float: left;
	width: 100%;
}
</style>

@include('includes.head')	 
@include('includes.header')	
 <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            @include('includes.navigation')	
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
					<div class="row p-b-30">
   <div class="row col-12 p-2" style="margin-left: 0px;">
      <div class="form-group col-md-6">
         <label class="col-form-label">Status</label>
         <select formcontrolname="status" required="" class="form-control ng-pristine ng-valid ng-touched">
            <option value="1">Active</option>
            <option value="0">Deactive</option>
         </select>
         <!---->
      </div>
      <div class="form-group col-md-6">
         <label class="col-form-label">Username</label><input type="text" formcontrolname="username" placeholder="Username" required="" class="form-control ng-pristine ng-invalid ng-touched">
         <div class="error"><span class="text-danger">Username is required</span></div>
         <!---->
      </div>
      <div class="form-group col-md-6">
         <label class="col-form-label">Email</label><input type="email" formcontrolname="email" placeholder="Email" duplicateemail="" required="" class="form-control ng-untouched ng-pristine ng-invalid"><!---->
      </div>
      <div class="form-group col-md-4">
         <label class="col-form-label">Phone</label><input type="number" formcontrolname="phone" placeholder="Phone" required="" class="form-control ng-untouched ng-pristine ng-invalid"><!---->
      </div>
      <div class="form-group col-md-2">
         <label class="col-form-label">Commission</label><input type="number" formcontrolname="commission" placeholder="Commission" required="" class="form-control ng-untouched ng-pristine ng-invalid"><!---->
      </div>
      <div class="form-group col-md-3">
         <label class="col-form-label">Password</label><input type="password" formcontrolname="password" placeholder="Password" required="" class="form-control ng-untouched ng-pristine ng-invalid"><!---->
      </div>
      <!---->
      <div class="form-group col-md-3"><label class="col-form-label">Is Force Change Password</label><input type="checkbox" formcontrolname="isForcePasswordChange" class="form-control ng-untouched ng-pristine ng-valid"></div>
      <!---->
      <div class="form-group col-md-3">
         <label class="col-form-label">Security Question</label>
         <select formcontrolname="security_question" class="form-control ng-untouched ng-pristine ng-invalid">
            <option value="">Select Your Security Question</option>
            <option>In what city or town was your first job?</option>
            <option>What is your favorite city name?</option>
            <!---->
         </select>
         <!---->
      </div>
      <div class="form-group col-md-3">
         <label class="col-form-label">Answer</label><input type="text" formcontrolname="answer" placeholder="Answer" required="" class="form-control ng-untouched ng-pristine ng-invalid"><!---->
      </div>
      <div class="form-group col-md-12">
         <label class="col-form-label">Devices</label>
         <table class="table">
            <thead>
               <tr>
                  <th>Device Name</th>
                  <th>Device Code</th>
                  <th>Device Location</th>
                  <th><button type="button" class="btn btn-sm btn-success float-right"><i class="fa fa-plus"></i></button></th>
               </tr>
            </thead>
            <tbody>
               <tr formarrayname="deviceIds" class="ng-untouched ng-pristine ng-invalid">
                  <td>
                     <select formcontrolname="device_name" class="form-control ng-untouched ng-pristine ng-invalid">
                        <option value="">-- Select Device --</option>
                        <!---->
                     </select>
                     <!---->
                  </td>
                  <td>
                     <input type="text" readonly="" formcontrolname="device_id" placeholder="Device Id" class="form-control ng-untouched ng-pristine ng-invalid"><!---->
                  </td>
                  <td>
                     <input type="text" readonly="" formcontrolname="device_loc" placeholder="Device Location" class="form-control ng-untouched ng-pristine ng-invalid"><!---->
                  </td>
                  <td><button type="button" class="btn btn-sm btn-danger float-right"><i class="fa fa-minus"></i></button></td>
                  <!---->
               </tr>
               <!---->
            </tbody>
         </table>
      </div>
      <div class="col-md-12"><input type="submit" value="Submit" class="btn btn-primary"></div>
   </div>
</div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
@include('includes.footer')		
<script type="text/javascript">

</script>