@include('includes.head')	 
@include('includes.header')	
 <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            @include('includes.navigation')	
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
				
			<!--	<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="icon-home"></i>
							<router-link :to="'/'" class="nav-link">
								<span class="title">Home</span>
							</router-link>	
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-tags"></i>
							<router-link :to="'/category'" class="nav-link">
								<span class="title">Manage Users</span>
							</router-link>	
						</li>		
					</ul>
				</div>    -->				
		<div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light ">
     				@if(Session::has('message'))
					<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
					@endif

                    <div class="manage-project-bx">
                        <div class="col-md-6">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <i class="icon-user"></i>
                                    <span class="caption-subject bold uppercase">Merchant List</span>
                                </div>
                            </div>
                            </div>

                            <div class="col-md-6 ortlet-body-bx">
                            <div class="btn-group">
                              <a class="btn sbold green" href="{{ url('admin/merchant') }}" style="margin-right:5px;"><i class="fa fa-plus"></i>Add New</a>
                             <!--  <a class="btn sbold red" href="{{ url('/admin/list-delete-merchant') }}"><i class="fa fa-trash-o"></i>Deleted Merchant</a> -->
                            </div>
                        </div> 
                    </div> 

                    <div class="portlet-body">
                                   
                                <table class="table table-bordered" id="users-table">
                                    <thead>
                                        <tr>
                                            <th>Name </th>
                                            <th>Email </th>
                                            <th>Mobile </th>
                                            <th>Status </th>
                                            <th>Action </th>
                                           
                                       
                                        </tr>
                                         </thead>
                                        <tbody>
                                            @foreach($users as $user_list)
                                            <tr>
                                                <td>{{$user_list['username']}}</td>
                                                <td>{{$user_list['email']}}</td>
                                                <td>{{$user_list['phone']}}</td>
                                                <td>
                                            @if(isset($user_list['status']) &&  $user_list['status']==1)
                                               Active
                                               @else
                                               Deactive
                                               @endif
                                           </td>
                                           <td><a href="#"> <i class="fa fa-edit"></i> </a> 
                                             <a href="#">  <i class="fa fa-trash red" style="color: red;"></i> </a>  </td>
                                             
                                             
                                            </tr>
                                            @endforeach
                                        </tbody>
                                   
                                </table>

                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
@include('includes.footer')		 
<script type="text/javascript">
   $(function() {
    $('#users-table').DataTable();

$(document).on('click','#user-delete',function(){
var id = $(this).attr('data-id');
var con = confirm("Are you sure you want to delete?");
if(con == false)
{
    return false;
}
$.ajax({
        type:"POST",
        url:"{{url('admin/delete-merchant')}}",
        data: {  "_token": "{{ csrf_token() }}","id": id },
        success:function(res){               
        if(res == 1){
         location.reload();
         alert("Merchant has been deleted successfully");
       }
     }
     });
});
});


   
</script>