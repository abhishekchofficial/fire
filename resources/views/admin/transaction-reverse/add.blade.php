<style type="text/css">
.user_form_group .form-group{
	float: left;
	width: 100%;
}
</style>

@include('includes.head')	 
@include('includes.header')	
 <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            @include('includes.navigation')	
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
					<div class="row">
						<div class="col-md-12">
							<div class="portlet light add-section">
								<div class="portlet-body form portlet-title">
									<div class="caption font-red-sunglo">
										<!-- <i class="icon-user font-red-sunglo"></i>  -->
										<span class="caption-subject bold uppercase">Add Merchant</span>
									</div>
								</div>
								@if ($errors->any())
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
								@endif
		
								<form role="form" action="{{url('admin/addMerchant')}}" method="POST" enctype="multipart/form-data">
								  {{csrf_field()}}
										<div class="form-body user_form_group">
											<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
												<div class="form-group">
									                <label for="inputPassword2" class="col-md-3 control-label">Status</label>
									                <div class="col-md-9">
											            <select class="form-control" name="is_active">
											                <option value="1">Active</option>
															<option value="0">Deactive</option>
											            </select>
											        </div>
									            </div>
											</div>	
											<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
												<div class="form-group">
									                <label for="username" class="col-md-3 control-label">Username</label>
									                <div class="col-md-9">
									                    <div class="input-icon">
									                        <!-- <i class="fa fa-user"></i> -->
									                        <input type="text" class="form-control" name="username" value="{{ old('username') }}"/>
									                    </div>
									                </div>
								                </div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
												<div class="form-group">
									                <label for="inputEmail12" class="col-md-3 control-label">Email</label>
									                <div class="col-md-9">
									                    <div class="input-icon">
									                        <!-- <i class="fa fa-envelope"></i> -->
									                        <input type="email" class="form-control" id="inputEmail12" placeholder="Email" name="email" value="{{ old('email') }}"> 
									                    </div>
									                </div>
									            </div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
												<div class="form-group">
									                <label for="phone" class="col-md-3 control-label">Phone</label>
									                <div class="col-md-9">
									                    <div class="input-icon">
									                        <!-- <i class="fa fa-phone"></i> -->
									                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" value="{{ old('phone') }}"> 
									                        </div>
									                </div>
									            </div>
											</div>								            
											<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
												<div class="form-group">
									                <label for="inputPassword" class="col-md-3 control-label">Password</label>
									                <div class="col-md-9">
									                    <div class="input-icon">
									                        <!-- <i class="fa fa-key"></i> -->
									                        <input type="password" name="password" class="form-control" id="password" placeholder="Password"> 
									                    </div>                                               
									                </div>
									            </div>
											</div>
											
																		
										</div>					
										<div class="form-actions noborder pull-right">
											<button type="submit" class="btn blue red">Submit</button> 
										</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
@include('includes.footer')		
<script type="text/javascript">

</script>