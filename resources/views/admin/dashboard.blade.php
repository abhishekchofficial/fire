@include('includes.head')	 
@include('includes.header')	
 <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            @include('includes.navigation')	
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
				
			<!--	<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="icon-home"></i>
							<router-link :to="'/'" class="nav-link">
								<span class="title">Home</span>
							</router-link>	
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-tags"></i>
							<router-link :to="'/category'" class="nav-link">
								<span class="title">Manage Users</span>
							</router-link>	
						</li>		
					</ul>
				</div>    -->				
		<div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light ">
     				@if(Session::has('message'))
					<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
					@endif

                    <div class="manage-project-bx">
                        <div class="col-md-6">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <i class="icon-user"></i>
                                    <span class="caption-subject bold uppercase">Dashboard</span>
                                </div>
                            </div>
                            </div>

                            <div class="col-md-6 ortlet-body-bx">
                            <div class="btn-group">
                              <!-- <a class="btn sbold green" href="{{ url('admin/inventory-add') }}" style="margin-right:5px;"><i class="fa fa-plus"></i>Add New</a> -->
                             <!--  <a class="btn sbold red" href="{{ url('/admin/list-delete-merchant') }}"><i class="fa fa-trash-o"></i>Deleted Merchant</a> -->
                            </div>
                        </div> 
                    </div> 

                    <div class="portlet-body">
                                   
                         <div class="row">
                            <div class="col-md-12">
                                <div class="chartjs-size-monitor">
                                    <div class="chartjs-size-monitor-expand">
                                        <div class=""></div>
                                    </div>
                                    <div class="chartjs-size-monitor-shrink">
                                        <div class=""></div>
                                    </div>
                                </div>
                                <div class="col-md-4 float-right">
                                    <select name="filter" class="form-control ng-valid ng-dirty ng-touched">
                                        <option value="daily">Daily</option>
                                        <option value="weekly">Weekly</option>
                                        <option value="monthly">Monthly</option>
                                        <option value="yearly">Yearly</option>
                                    </select>
                                </div>
                                <canvas _ngcontent-tok-c39="" basechart="" style="display: block; width: 1059px; height: 529px;" class="chartjs-render-monitor" width="1059" height="529"></canvas>
                            </div>
                        </div>

                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
@include('includes.footer')		 
