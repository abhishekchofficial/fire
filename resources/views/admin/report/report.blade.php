@include('includes.head')	 
@include('includes.header')	
 <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            @include('includes.navigation')	
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
				
				
		<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<div class="table-responsive">
					<div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
						<div class="dt-buttons">
							<button class="dt-button buttons-copy buttons-html5" tabindex="0" aria-controls="zero_config" type="button">
								<span>Copy</span>
							</button>
							<button class="dt-button buttons-excel buttons-html5" tabindex="0" aria-controls="zero_config" type="button">
								<span>Excel</span>
							</button>
							<button class="dt-button buttons-csv buttons-html5" tabindex="0" aria-controls="zero_config" type="button">
								<span>CSV</span>
							</button>
							<button class="dt-button buttons-pdf buttons-html5" tabindex="0" aria-controls="zero_config" type="button">
								<span>PDF</span>
							</button>
						</div>
						<div id="zero_config_filter" class="dataTables_filter">
							<label>Search:
								<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="zero_config">
								</label>
							</div>
							<table id="zero_config" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="zero_config_info">
								<thead>
									<tr role="row">
										<th class="sorting_asc" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-sort="ascending" aria-label="#: activate to sort column descending" style="width: 9px;">#</th>
										<th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Merchant Name: activate to sort column ascending" style="width: 151px;">Merchant Name</th>
										<th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Email: activate to sort column ascending" style="width: 151px;">Email</th>
										<th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Amount: activate to sort column ascending" style="width: 51px;">Amount</th>
										<th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Commission: activate to sort column ascending" style="width: 77px;">Commission</th>
										<th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Commission Amount: activate to sort column ascending" style="width: 77px;">Commission Amount</th>
										<th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Payable Amount: activate to sort column ascending" style="width: 51px;">Payable Amount</th>
										<th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Transaction Id: activate to sort column ascending" style="width: 127px;">Transaction Id</th>
										<th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Transaction Date: activate to sort column ascending" style="width: 73px;">Transaction Date</th>
										<th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1" colspan="1" aria-label="Transaction Status: activate to sort column ascending" style="width: 73px;">Transaction Status</th>
									</tr>
								</thead>
								<tbody>
									<!---->
									<tr role="row" class="odd">
										<td class="sorting_1">1</td>
										<td></td>
										<td></td>
										<td>0.1</td>
										<td>%</td>
										<td>0.00</td>
										<td>0.10</td>
										<td>5212942282205230</td>
										<td>2021-03-20</td>
										<td>Transaction Accepted, A Callback will be sent on the final stage</td>
									</tr>
									<tr role="row" class="even">
										<td class="sorting_1">2</td>
										<td></td>
										<td></td>
										<td>20.0</td>
										<td>%</td>
										<td>0.00</td>
										<td>20.00</td>
										<td>362efd59-a142-45c2-998a-13273ddc2c18</td>
										<td>2020-11-30</td>
										<td> Request Failed. Try again later: Could not verify wallet to debit</td>
									</tr>
									<tr role="row" class="odd">
										<td class="sorting_1">3</td>
										<td></td>
										<td></td>
										<td>30.0</td>
										<td>%</td>
										<td>0.00</td>
										<td>30.00</td>
										<td>2d0c8883-181a-4a66-97be-99d926197ef7</td>
										<td>2020-12-03</td>
										<td> Request Failed. Try again later: Could not verify wallet to debit</td>
									</tr>
									<tr role="row" class="even">
										<td class="sorting_1">4</td>
										<td></td>
										<td></td>
										<td>0.1</td>
										<td>%</td>
										<td>0.00</td>
										<td>0.10</td>
										<td>EDE74DBB870B4442</td>
										<td>2021-05-19</td>
										<td>Transaction Successful</td>
									</tr>
									<tr role="row" class="odd">
										<td class="sorting_1">5</td>
										<td>pradeep.singh@gmail.com</td>
										<td>pradeep.singh@gmail.com</td>
										<td>0.1</td>
										<td>1.5%</td>
										<td>0.00</td>
										<td>0.10</td>
										<td>5270944813601944</td>
										<td>2021-05-22</td>
										<td>Transaction Failed</td>
									</tr>
									<tr role="row" class="even">
										<td class="sorting_1">6</td>
										<td>pradeep.singh@gmail.com</td>
										<td>pradeep.singh@gmail.com</td>
										<td>0.1</td>
										<td>1.5%</td>
										<td>0.00</td>
										<td>0.10</td>
										<td>5220828130985605</td>
										<td>2021-05-22</td>
										<td>Transaction Failed</td>
									</tr>
									<tr role="row" class="odd">
										<td class="sorting_1">7</td>
										<td>pradeep.singh@gmail.com</td>
										<td>pradeep.singh@gmail.com</td>
										<td>0.1</td>
										<td>1.5%</td>
										<td>0.00</td>
										<td>0.10</td>
										<td>5221469950112962</td>
										<td>2021-05-22 14:00:00</td>
										<td>Transaction Failed</td>
									</tr>
									<tr role="row" class="even">
										<td class="sorting_1">8</td>
										<td>Bemo</td>
										<td>Bemopay@gmail.com</td>
										<td>10.0</td>
										<td>2.5%</td>
										<td>0.25</td>
										<td>9.75</td>
										<td>5292241727972437</td>
										<td>2021-05-27</td>
										<td>Transaction Failed</td>
									</tr>
									<tr role="row" class="odd">
										<td class="sorting_1">9</td>
										<td>Bemo</td>
										<td>Bemopay@gmail.com</td>
										<td>0.1</td>
										<td>2.5%</td>
										<td>0.00</td>
										<td>0.10</td>
										<td>5212053227663725</td>
										<td>2021-05-24</td>
										<td>Transaction Failed</td>
									</tr>
									<tr role="row" class="even">
										<td class="sorting_1">10</td>
										<td>Bemo</td>
										<td>Bemopay@gmail.com</td>
										<td>52.0</td>
										<td>2.5%</td>
										<td>1.30</td>
										<td>50.70</td>
										<td>7ed20686-40ab-4da8-b187-9444edfd106b</td>
										<td>2020-10-28</td>
										<td> Request Failed. Try again later: </td>
									</tr>
								</tbody>
							</table>
							<div class="dataTables_info" id="zero_config_info" role="status" aria-live="polite">Showing 1 to 10 of 20 entries</div>
							<div class="dataTables_paginate paging_simple_numbers" id="zero_config_paginate">
								<ul class="pagination">
									<li class="paginate_button page-item previous disabled" id="zero_config_previous">
										<a href="#" aria-controls="zero_config" data-dt-idx="0" tabindex="0" class="page-link">Previous</a>
									</li>
									<li class="paginate_button page-item active">
										<a href="#" aria-controls="zero_config" data-dt-idx="1" tabindex="0" class="page-link">1</a>
									</li>
									<li class="paginate_button page-item ">
										<a href="#" aria-controls="zero_config" data-dt-idx="2" tabindex="0" class="page-link">2</a>
									</li>
									<li class="paginate_button page-item next" id="zero_config_next">
										<a href="#" aria-controls="zero_config" data-dt-idx="3" tabindex="0" class="page-link">Next</a>
									</li>
								</ul>
							</div>
						</div>
						<!---->
					</div>
				</div>
			</div>
		</div>
	</div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
@include('includes.footer')		 
<script type="text/javascript">
   $(function() {
    $('#users-table').DataTable();

$(document).on('click','#user-delete',function(){
var id = $(this).attr('data-id');
var con = confirm("Are you sure you want to delete?");
if(con == false)
{
    return false;
}
$.ajax({
        type:"POST",
        url:"{{url('admin/delete-merchant')}}",
        data: {  "_token": "{{ csrf_token() }}","id": id },
        success:function(res){               
        if(res == 1){
         location.reload();
         alert("Merchant has been deleted successfully");
       }
     }
     });
});
});


   
</script>