<style type="text/css">
.user_form_group .form-group{
	float: left;
	width: 100%;
}
</style>

@include('includes.head')	 
@include('includes.header')	
 <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            @include('includes.navigation')	
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
					<div class="row">
						<div class="col-md-12">
							<div class="portlet light add-section">
								<div class="portlet-body form portlet-title">
									<div class="caption font-red-sunglo">
                                        <!-- <i class="icon-user font-red-sunglo"></i>  -->
                                        <span class="caption-subject bold uppercase">
                                        @if($user->first_name)
                                            {{$user->first_name}} {{$user->last_name}}
                                        @else
                                            User
                                        @endif
                                        Details</span>
									</div>
								</div>
								@if ($errors->any())
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
								@endif
		
								<ul class="list-group">
                                    <li class="list-group-item">Current Membership Plan:
                                        @isset($active_membership_details)
                                            @if($active_membership_details->membership_plan_name)
                                                {{$active_membership_details->membership_plan_name}}
                                            @else
                                                Free Plan
                                            @endif
                                        @else
                                            Free Plan
                                        @endisset
                                    </li>
                                </ul>

                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <!-- <i class="icon-envelope-open"></i> -->
                                        <span class="caption-subject bold uppercase">Invoices</span>
                                    </div>
                                </div>
                                <table class="table table-bordered" id="users-details-table">
                                    <thead>
                                        <tr>
                                            <th>S.no</th>
                                            <th>Txn. ID</th>
                                            <th>Type</th>
                                            <th>Plan Name</th>
                                            <th>Price($)</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>
                                        @foreach($all_pack_details as $pack)
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>{{$pack->transaction_id}}</td>
                                            <td>
                                                @if($pack->type == 'Package')
                                                    Membership
                                                @else
                                                    {{$pack->type}}
                                                @endif
                                            </td>
                                            <td>
                                                @if($pack->membership_plan_name)
                                                    {{$pack->membership_plan_name}} Plan
                                                @else
                                                    @if($pack->ad_type == 0)
                                                        Featured
                                                    @elseif($pack->ad_type == 1)
                                                        Top Result
                                                    @else
                                                        -
                                                    @endif
                                                @endif
                                            </td>
                                            <td>{{$pack->membership_plan_price}}</td>
                                            <td>{{Carbon\Carbon::parse($pack->start_date)->format('Y-m-d')}}</td>
                                            <td>{{Carbon\Carbon::parse($pack->end_date)->format('Y-m-d')}}</td>
                                            <td>{{ucwords($pack->payment_status)}}</td>
                                        </tr>
                                        <?php $i++; ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light add-section">
                                <div class="portlet-body form portlet-title">
                                </div>

                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <!-- <i class="icon-envelope-open"></i> -->
                                        <span class="caption-subject bold uppercase">Wishlist</span>
                                    </div>
                                </div>
                                <table class="table table-bordered" id="users-details-table">
                                    <thead>
                                        <tr>
                                            <th>S.no</th>
                                            <th>Product name</th>
                                            <th>Product SKU</th>
                                            <th>Product Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>
                                        @foreach($wishlist as $w_key => $w_value)
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>{{$w_value->product_name}}</td>
                                            <td>
                                                @if($w_value->sku)
                                                {{$w_value->sku}}
                                                @else
                                                -
                                                @endif
                                            </td>
                                            <td>{{$w_value->regular_price_usd}}</td>
                                        </tr>
                                        <?php $i++; ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
@include('includes.footer')		
<script type="text/javascript">
$(function() {
    $('#users-details-table').DataTable();
});
</script>