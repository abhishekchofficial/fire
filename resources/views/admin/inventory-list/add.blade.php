<style type="text/css">
.user_form_group .form-group{
	float: left;
	width: 100%;
}
</style>

@include('includes.head')	 
@include('includes.header')	
 <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            @include('includes.navigation')	
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
					<div class="row" style="margin-left: 0px !important;">
					   <div class="col-12">
					      <div class="card">
					         <form novalidate="" id="loginform" class="form-horizontal m-t-20 ng-untouched ng-pristine ng-invalid">
					            <div class="row p-b-30">
					               <div class="row col-12 p-2" style="margin-left: 0px;">
					                  <div class="form-group col-md-6">
					                     <label class="col-form-label">Name</label><input type="text" formcontrolname="name" placeholder="Device Name" required="" class="form-control ng-untouched ng-pristine ng-invalid"><!---->
					                  </div>
					                  <div class="form-group col-md-6">
					                     <label class="col-form-label">Device Code</label><input type="email" formcontrolname="code" placeholder="Device Code" required="" class="form-control ng-untouched ng-pristine ng-invalid"><!---->
					                  </div>
					                  <div class="form-group col-md-6">
					                     <label class="col-form-label">Location ID</label><input type="text" formcontrolname="location" placeholder="Device Location" required="" class="form-control ng-untouched ng-pristine ng-invalid"><!---->
					                  </div>
					                  <div class="form-group col-md-6">
					                     <label class="col-form-label">IMEI No.</label><input type="text" formcontrolname="imei_no" placeholder="Device IMEI No." class="form-control ng-untouched ng-pristine ng-invalid"><!---->
					                  </div>
					                  <div class="form-group col-md-6">
					                     <label class="col-form-label">Status</label>
					                     <select formcontrolname="status" class="form-control ng-untouched ng-pristine ng-valid">
					                        <option value="1">Active</option>
					                        <option value="0">Deactive</option>
					                     </select>
					                     <!---->
					                  </div>
					                  <!---->
					                  <div class="col-md-12"><input type="submit" value="Submit" class="btn btn-primary"></div>
					               </div>
					            </div>
					         </form>
					      </div>
					   </div>
					</div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
@include('includes.footer')		
<script type="text/javascript">

</script>