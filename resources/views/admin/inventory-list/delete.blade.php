@include('includes.head')    
@include('includes.header') 
 <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            @include('includes.navigation') 
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                
            <!--    <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <router-link :to="'/'" class="nav-link">
                                <span class="title">Home</span>
                            </router-link>  
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <i class="fa fa-tags"></i>
                            <router-link :to="'/category'" class="nav-link">
                                <span class="title">Manage Users</span>
                            </router-link>  
                        </li>       
                    </ul>
                </div>    -->               
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light ">
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif

                    <div class="manage-project-bx">
                        <div class="col-md-6">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <i class="icon-user"></i>
                                    <span class="caption-subject bold uppercase">Deleted Users</span>
                                </div>
                            </div>
                            </div>

                            <div class="col-md-6 ortlet-body-bx">
                            <div class="btn-group">
                              <a class="btn sbold green" href="{{ url('admin/add-users') }}" style="margin-right:5px;"><i class="fa fa-plus"></i>Add New</a>
                            </div>
                        </div> 
                    </div> 

                    <div class="portlet-body">
                                   
                                <table class="table table-bordered" id="users-table">
                                    <thead>
                                        <tr>
                                            <th>User Name</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>

                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
@include('includes.footer')     
<script type="text/javascript">
   $(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{!! url('/admin/list-delete-users/deletedUserList') !!}",
        columns: [
            { data: 'username', name: 'username' },
            { data: 'mobile', name: 'mobile' },
            { data: 'email', name: 'email' },
            { data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
});

$(document).on('click','#user-remove',function(){
var id = $(this).attr('data-id');
var con = confirm("Are you sure you want to delete parmanently?");
if(con == false)
{
    return false;
}
$.ajax({
        type:"POST",
        url:"{{url('admin/pdelete-user')}}",
        data: {  "_token": "{{ csrf_token() }}","id": id },
        success:function(res){               
        if(res == 1){
         location.reload();
         alert("User has been deleted successfully");
       }
     }
     });
});

$(document).on('click','#user-restore',function(){
var id = $(this).attr('data-id');
var con = confirm("Are you sure you want to restore?");
if(con == false)
{
    return false;
}
$.ajax({
        type:"POST",
        url:"{{url('admin/restore-user')}}",
        data: {  "_token": "{{ csrf_token() }}","id": id },
        success:function(res){               
        if(res == 1){
         location.reload();
         alert("User has been restore successfully");
       }
     }
     });
});
</script>