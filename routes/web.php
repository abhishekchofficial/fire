<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('firebase','FirebaseController@index');

Route::get('firebase-get-data', 'FirebaseController@getData');

Route::group(['prefix' => 'admin'], function() {

    Route::get('/home', 'InventoryListController@home');
    Route::get('/reports', 'InventoryListController@report');

	 /*Merchant*/
	Route::get('/', 'MerchantListController@index');
    Route::get('merchant-list', 'MerchantListController@index');
    Route::any('merchant', 'MerchantListController@addMerchant');
     /*Merchant*/

    /*Inventory*/
    Route::get('inventory-list', 'InventoryListController@index');
    Route::get('inventory-add', 'InventoryListController@add');
    /*Inventory*/

    Route::get('transaction-reverse', 'TransactionReverseController@index');
    
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
