<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Kreait\Firebase;

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

use Kreait\Firebase\Database;
use Kreait\Firebase\Firestore;
use Google\Cloud\Firestore\FirestoreClient;

class InventoryListController extends Controller
{

       protected $db;  
   // public function __construct() {  
   //   $this->db = app('firebase.firestore')->database();  
   // } 


    // --------------- [ Listing Data ] ------------------
    
    // -------------------- [ Insert Data ] ------------------
    public function home() {
      return view('admin.dashboard');
    }

    public function add() {
      return view('admin.inventory-list.add');
    }
    
    public function report() {
      return view('admin.report.report');
    }


    public function index() {
    	$data = app('firebase.firestore')->database()->collection('poses')->documents(); 
    	$inventories = [];
		foreach ($data as $document) {
			array_push($inventories, $document->data());
				
				
		}
		//echo "<pre>";print_r($users);
		return view('admin.inventory-list.list', compact('inventories'));
    }

    public function addMerchant(Request $request) {
    	if ($request->isMethod('post')) {

    	}else{
    		return view('admin.merchant-list.add');
    	}
		//echo "<pre>";print_r($users);
		return view('admin.merchant-list.list', compact('users'));
    }
}
